package s05;

public class Ex2 {

    private static int power(int b, int p) { // PRE: p > 0
        int res = 1, n = p;
        int x = b;              //
        while (n>0) {           //
            if (n % 2 == 0) {   //
                x = x*x;        //
                n = n/2;        //
            } else {            //
                res = res * x;  //
                n--;            //
            }
            //
        }
        return res; //
    }

    public static void main(String[] args){
        System.out.println(power(3,9));
    }






}

