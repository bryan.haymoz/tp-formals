package s05;
public class Ex1 {

    private static void ptA() {
        int e=2, f=3;

        int c = Math.abs(e-f) % 2; // |e-f| % 2 == c
        if (e>0) {
            e = e-1; // |(e+1) - f| % 2 == c
            f = f+1; // |(e+1) - (f-1)| % 2 == c
            // |e - f + 2| % 2 == c
        } else {
            e = e+1; // |(e-1) - f| % 2 == c
            f = f-1; // |(e-1) - (f+1)| % 2 == c
        }            // |e - f - 2| % 2 == c
        // |e-f| % 2 == c
    }

    private static void ptB() {
        //e*f -> non
        //e-f -> oui
        //e^f -> non
        //e+f -> oui

    }
}

