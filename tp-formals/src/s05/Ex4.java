package s05;

import java.util.Random;

public class Ex4 {
  // =================================
  // For any:  int i;  F f,g;
  //   i == new F(i).a()
  //   f.s(g).a() == f.a()+g.a()
  static class F {
    private int i;
    public F(int i) {
      this.i = i;
    }
    public int a()  {
      return i; // TODO
    }
    /*@ ensures \old(a()) == a() @*/
    public F s(F y) {
      return new F(a() + y.a()); // TODO;
    }
  }
  // =================================

  //------------------------------------------------------------------

  public static boolean areAssertionsEnabled() {
    int ec=0;
    assert (ec=1) == 1;
    return ec == 1;
  }

  static Random rnd=new Random();

  // PRE: 0 < absMax < MAX_VALUE/2
  static int rndIntIn(int absMax) {
    return rnd.nextInt(2*absMax)-absMax;
  }

  static void playWithClassF() {
    int nOperations = 1000;
    int max = 1000;
    F f=new F(rndIntIn(max));
    F g=new F(rndIntIn(max));

    for(int i=0; i<nOperations; i++) {
      assert f.s(g).a() == f.a()+g.a();
      int v = rndIntIn(max);
      F h = new F(v);
      assert v == h.a();
      int fa = f.a();
      int ga = g.a();
      switch(rnd.nextInt(3)) {
        case 0: h = f.s(g);
        case 1: h = g.s(f);
        case 2: break;
      }
      assert fa == f.a();
      assert ga == g.a();
      if(rnd.nextBoolean()) f = h;
      else g = h;
    }
  }

  public static void main(String[] args) {
    if(!areAssertionsEnabled()) {
      System.out.println("Please enable assertions, with '-ea' VM option !!");
      System.exit(-1);
    }
    playWithClassF();
    System.out.println("End of demo!");
  }

}
