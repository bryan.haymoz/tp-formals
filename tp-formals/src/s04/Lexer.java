package s04;

public class Lexer {
  private String crtToken = "";
  private String whole;
  private int nextCharIndex=0;  // index of the char following crtToken

  public Lexer(String s) {
    whole = s;
    goToNextSymbol();
  }

  public String crtSymbol() {   // returns "" if the end is reached
    return crtToken;
  }

  private boolean noMoreChars() {
    return nextCharIndex>=whole.length();
  }

  private void trimSpaces() {
    if (noMoreChars()) return;
    char c = whole.charAt(nextCharIndex);
    while (Character.isWhitespace(c)) { 
      nextCharIndex++;
      if (noMoreChars()) return;
      c = whole.charAt(nextCharIndex);
    }
    return;    
  }

  public void goToNextSymbol() {
    crtToken = "";
    trimSpaces();
    if (noMoreChars()) return;
    char c = whole.charAt(nextCharIndex);
    // .....
    // TODO - A COMPLETER...
    // Selon que le prochain caractère est :
    // - une lettre : 
    //     grouper les lettres consécutives
    // - un chiffre : 
    //     grouper les chiffres consécutifs
    // - autre chose : 
    //     prendre juste ce caractère

    // Contrôle tant que caractère est une lettre
    while(Character.isLetter(c)){
      crtToken += c;
      nextCharIndex++;
      if(noMoreChars()) return;
      c = whole.charAt(nextCharIndex);
      if(Character.isWhitespace(c) || !Character.isLetter(c) || noMoreChars()) return;
    }

    //Contrôle tant que caractère est un nombre
    while(Character.isDigit(c)){
      crtToken += c;
      nextCharIndex++;
      if(noMoreChars()) return;
      c = whole.charAt(nextCharIndex);
      if(Character.isWhitespace(c) || !Character.isDigit(c) || noMoreChars()) return;
    }

    //Si autre chose ne prendre que le caractère
    if(Character.isWhitespace(c)) return;
    nextCharIndex++;
    crtToken +=c;
  }

  public boolean isNumber() {
    if (crtToken.length()==0)return false;
    for(int j=0; j<crtToken.length(); j++)
      if (! Character.isDigit(crtToken.charAt(j)))
        return false;
    return true;
  }

  public boolean isIdent() {
    if (crtToken.length()==0)return false;
    for(int j=0; j<crtToken.length(); j++)
      if (! Character.isLetter(crtToken.charAt(j)))
        return false;
    return true;
  }

  public boolean isOpeningParenth() { return crtToken.equals("("); }
  public boolean isClosingParenth() { return crtToken.equals(")"); }
  public boolean isPlus()           { return crtToken.equals("+"); }
  public boolean isMinus()          { return crtToken.equals("-"); }
  public boolean isStar()           { return crtToken.equals("*"); }
  public boolean isSlash()          { return crtToken.equals("/"); }

  // returns 0 if crtSymbol is not a number
  public int intFromSymbol() { 
    if (! isNumber()) 
      return 0;
    return Integer.parseInt(crtToken);
  }

}
