package s04;

import java.util.HashMap;

public class Interpreter {
    private static Lexer lexer;
    // TODO - A COMPLETER (ex. 3)
    private static HashMap<String, Integer> variables = new HashMap<>();

    public static int evaluate(String e) throws ExprException {
        lexer = new Lexer(e);
        int res = parseExpr();
        // test if nothing follows the expression...
        if (lexer.crtSymbol().length() > 0)
            throw new ExprException("bad suffix");
        return res;
    }

    private static int parseExpr() throws ExprException {
        int res = 0;
        res = parseTerm();
        boolean stop = false;
        while (!stop) {
            if (lexer.isPlus()) {
                lexer.goToNextSymbol();
                res += parseTerm();
            } else if (lexer.isMinus()) {
                lexer.goToNextSymbol();
                res -= parseTerm();
            } else {
                stop = true;
            }
        }
        return res;
    }

    private static int parseTerm() throws ExprException {
        int res = 0;
        res = parseFact();
        boolean stop = false;
        while (!stop) {
            if (lexer.isStar()) {
                lexer.goToNextSymbol();
                res *= parseTerm();
            } else if (lexer.isSlash()) {
                lexer.goToNextSymbol();
                res /= parseTerm();
            } else {
                stop = true;
            }
        }
        return res;
    }

    private static int parseFact() throws ExprException {
        int res = 0;
        if (lexer.isOpeningParenth()) {
            lexer.goToNextSymbol();
            res = parseExpr(); //First possibility ( Expr )
            if (!lexer.isClosingParenth()) throw new ExprException("Need closing parenthesis");
            lexer.goToNextSymbol();
            if(lexer.isIdent()) {
                variables.put(lexer.crtSymbol(), res);
                lexer.goToNextSymbol();
            }
        } else if (lexer.isNumber()) {
            res = lexer.intFromSymbol(); //Second possibility Int
            lexer.goToNextSymbol();

        } else if (lexer.isIdent()) {
            String function = lexer.crtSymbol(); //Third possibility Ident( Expr )
            lexer.goToNextSymbol();
            if (!lexer.isOpeningParenth()) {
                if(!variables.containsKey(function)){
                    throw new ExprException("Doesn't contain variable");
                }
                res = variables.get(function);
            } else {
                lexer.goToNextSymbol();
                int arg = parseExpr();
                res = applyFct(function, arg);
                if (!lexer.isClosingParenth()) throw new ExprException("Need a closing parenth");
                lexer.goToNextSymbol();
                if(lexer.isIdent()){
                    variables.put(lexer.crtSymbol(), res);
                    lexer.goToNextSymbol();
                }
            }
        } else {
            throw new ExprException("No way possible for fact");
        }
        return res;
    }

    private static int applyFct(String fctName, int arg) throws ExprException {
        switch (fctName) {
            case "abs":
                return Math.abs(arg);
            case "sqr":
                return (int) Math.pow(arg, 2);
            case "cube":
                return (int) Math.pow(arg, 3);
            case "sqrt":
                if(arg < 0) throw new ExprException("sqrt() only positive number");
                return (int) Math.sqrt(arg);
            default:
                throw new ExprException("Not a kown fctName");
        }
    }
}
