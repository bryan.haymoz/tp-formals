package s03;

/**
 * <p>Description: tests the FSM class</p>
 * @author Stotzer, HEIA-FR
 * @version 1.0
 */
import java.util.*;
import java.util.Set;

public class MyFSMTest {

  public static String generateWrongWord(int nbOfStates, Random r) {
    if (r.nextBoolean())
      return generateCorrectWord(nbOfStates, r) + 'c';
    else
      return generateCorrectWord(nbOfStates, r) + 'b';
  }

  public static String generateCorrectWord(int nbOfStates, Random r) {
    String letters = "";
    //int maxWordSize = r.nextInt(20);
    int nextStep;
    for (int i = 0; i < nbOfStates - 1; i++) {
      while (r.nextBoolean() || r.nextBoolean()) {
        nextStep = r.nextInt(2);
        switch (nextStep) {
          case 0:
            letters += 'a';
            break;
          case 1:
            letters += "bc";
            break;
          case 2:
            letters += "cb";
            break;
        }
      }
      letters += 'b';
    }
    return letters;
  }

  public static boolean testWord(FSM autom, String letters,
                                 boolean expectedResult) {
    int[] word = new int[letters.length()];
    for (int j = 0; j < letters.length(); j++)
      word[j] = letters.charAt(j) - 'a';
    boolean res = autom.accepts(word);
    if (expectedResult != res)
      if (expectedResult)
        System.out.println("'" + letters + "' should be accepted");
      else
        System.out.println("'" + letters + "' should be refused");
    return res;
  }

  public static boolean testCorrectWords(FSM autom, Random r, int nStates,
                                         int nWords) {
    // Following words should be accepted
    boolean res = true;
    String letters = "";

    res = testWord(autom, "c", true) && res;
    for (int i = 0; i < nStates - 1; i++)
      letters += 'b';
    res = testWord(autom, letters, true) && res;
    for (int i = 0; i < nWords; i++) {
      letters = generateCorrectWord(nStates, r);
      res = testWord(autom, letters, true) && res;
    }
    return res;
  }

  public static boolean testWrongWords(FSM autom, Random r, int nStates,
                                       int nWords) {
    // Following words should be refused
    boolean res = true;
    String letters = "";

    res = !testWord(autom, "", false) && res;
    res = !testWord(autom, "a", false) && res;
    for (int i = 0; i < nWords; i++) {
      letters = generateWrongWord(nStates, r);
      res = !testWord(autom, letters, false) && res;
    }
    return res;
  }

  public static int[][] createTransitions(int nStates) {
    // creates a circular graph with following transitions
    // a: stay in current state
    // b: go to next state
    // c: go to previous state
    // the last state is the accepting state

    int[][] transitions = new int[nStates][3];
    for (int i = 0; i < nStates; i++) {
      transitions[i][0] = i;
      transitions[i][1] = (i + 1) % nStates;
      transitions[i][2] = (nStates + i - 1) % nStates;
    }
    return transitions;
  }

  public static void main(String[] args) {
    System.out.println("Testing FSM");
    Random r = new Random();
    int nStates = 3; // Nbr of States in the graph
    int nWordsToTest = 100;
    boolean res = true;

    // create graph
    int[][] transitions = createTransitions(nStates);
    Set<Integer> acceptingStates = new HashSet<>();
    acceptingStates.add(nStates - 1);
    FSM autom = new FSM(transitions, acceptingStates);

    // test a few words
    res = testWrongWords(autom, r, nStates, nWordsToTest);
    res = testCorrectWords(autom, r, nStates, nWordsToTest) && res;
    if (res)
      System.out.println("\nTest passed successfully");
    else
      System.out.println("\nTest failed; there is a bug");
  }
}