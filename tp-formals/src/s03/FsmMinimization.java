package s03;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Random;
import java.util.Set;

public class FsmMinimization {
  static public class EquivalenceRelation {
    /** internally we only use [i][j] cells where i<j*/
    private final boolean[][] equiv;
    
    /** Initially all elements are equivalent */
    public EquivalenceRelation(int size) {
      this.equiv=new boolean[size][size];
      for(int i=0; i<size; i++)
        for(int j=i+1; j<size; j++)
          equiv[i][j]=true;
    }  
    
    public int size() {
      return equiv.length;
    }
    
    public boolean areEquivalent(int i, int j) {
      if(i==j) return true;
      return equiv[Math.min(i, j)][Math.max(i, j)];
    }
    
    public void markAsEquivalent(int i, int j) {
      equiv[Math.min(i, j)][Math.max(i, j)]=true;
    }
    
    public void markAsNonEquivalent(int i, int j) {
      if(i==j) 
        throw new RuntimeException(i+" is necessarily equivalent to itself...");
      equiv[Math.min(i, j)][Math.max(i, j)]=false;
    }
    
    public int smallestEquivalent(int i) {
      for(int j=0; j<i; j++)
        if(areEquivalent(j,i)) return j;
      return i;
    }
    
    public void removeAllEquivalences() {
      for(int i=0; i<size(); i++)
        for(int j=i+1; j<size(); j++)
          equiv[i][j]=false;
    }
    
    public boolean isValid() {
      for(int i=0; i<size(); i++) {
        Set<Integer> eqClass=new HashSet<>();
        for(int j=0; j<size(); j++)
          if(areEquivalent(i, j)) {
            for(int a:eqClass)
              if(!areEquivalent(a, j)) return false;
            eqClass.add(i);
          }
      }
      return true;
    }
  }
  //============================================================
  
  public static EquivalenceRelation equivalences(int[][] transitions, 
                                            Set<Integer> acceptingStates) {
    int nStates = transitions.length; //Nombre états
    int nSymbols = transitions[0].length; //Nombre symboles

    EquivalenceRelation equivalenceRelation = new EquivalenceRelation(nStates);

    //1. considérer d'abord que tous les états sont équivalents
    for (int i = 0; i < nStates; i++) {
      for (int j = 0; j < nStates; j++) {
        equivalenceRelation.markAsEquivalent(i,j); //marquer comme équivalent
      }
    }

    //2. Appliquer la règle(a) autant que possible
    //Règle a -> un état acceptant n'est pas équivalent à un état non acceptant
    for (int i = 0; i < nStates; i++) {
      if(!acceptingStates.contains(i)){ // si i est un état non acceptant
        for (int j = 0; j < nStates; j++) {
          if(i == j || !acceptingStates.contains(j)) continue;// si même état ou que j est un état non acceptant prochaine itération
          equivalenceRelation.markAsNonEquivalent(i,j); //si i est un état non acceptant et que j est un état acceptant marquer comme non équivalent
        }
      }
    }

    //3. Trouver une paire (x,y) ou la règle (b) s'applique, et adapter en conséquence
    //Règle b -> deux états x et y ne sont pas équivalents s'il y a un symbole pour lequel
    // les transitions respectives nous mènent vers deux états non équivalents
    boolean stop;
    do{
      stop = true;
      for (int x = 0; x < nStates; x++) { //état x
        for (int y = 0; y < nStates; y++) { //état y
          if (x == y) continue; //si x et y même état prochain itération
          for (int s = 0; s < nSymbols; s++) { //symboles
            //Vérifier règle b
            if (!equivalenceRelation.areEquivalent(transitions[x][s], transitions[y][s]) && equivalenceRelation.areEquivalent(x, y)) {
              equivalenceRelation.markAsNonEquivalent(x, y);
              stop = false; //4. Répéter 3. tant que c'est possible
            }
          }
        }
      }
    }while(!stop);

    return equivalenceRelation;
  }

  public static FSM minimized(int[][] transitions, Set<Integer> acceptingStates) {
    int nStates = transitions.length; 
    int nSymbols = transitions[0].length;
    EquivalenceRelation r=equivalences(transitions, acceptingStates);
    if (!r.isValid()) throw new RuntimeException("Oups: not an equivalence relation");

    // renumber equivalence class representatives as 0,1,2...;  old->new
    Map<Integer,Integer> renumber=new HashMap<>();
    int nStatesMin=0;
    for(int i=0; i<nStates; i++) {
      int k=r.smallestEquivalent(i);
      int remapped;
      if(renumber.containsKey(k)) remapped=renumber.get(k);
      else remapped=nStatesMin++;
      renumber.put(i, remapped);
    }
    
    // rebuild transition function
    int[][] transitionsMin=new int[nStatesMin][nSymbols];
    for(int i=0; i<nStates; i++)
      for(int s=0; s<nSymbols; s++)
        transitionsMin[renumber.get(i)][s]=renumber.get(transitions[i][s]);
    
    // rebuild set of accepting states
    Set<Integer> acceptingStatesMin=new HashSet<>();
    for(int i:acceptingStates)
      acceptingStatesMin.add(renumber.get(i));

    return new FSM(transitionsMin, acceptingStatesMin);
  }
  
  //---------------------------------------------------------
  public static int[][] rndTransitions(int nStates, int nSymbols, Random rnd) {
    int[][] t=new int[nStates][nSymbols];
    for(int i=0; i<nStates; i++)
      for(int s=0; s<nSymbols; s++)
        t[i][s]=rnd.nextInt(nStates);
    return t;
  }
  
  public static Set<Integer> rndAcceptingStates(int nStates, Random rnd) {
    Set<Integer> a=new HashSet<>();
    int k=rnd.nextInt(nStates+1);
    while(a.size()<k)
      a.add(rnd.nextInt(nStates));
    return a;
  }

  public static int[] rndWord(int nSymbols, int maxLength, Random rnd) {
    int len=rnd.nextInt(maxLength+1);
    int[] w=new int[len];
    for(int i=0; i<len; i++)
      w[i]=rnd.nextInt(nSymbols);
    return w;
  }
  
  public static boolean seemEquivalent(FSM a, FSM b) {
    if(a.nSymbols()!=b.nSymbols()) {
      return false; // not the same alphabet...
    }
    Random rnd=new Random();
    int nWords=10_000;
    int maxWordLen=50;
    while(nWords-- >0) {
      int[] word=rndWord(a.nSymbols(), maxWordLen, rnd);
      if(a.accepts(word) != b.accepts(word)) {
        return false;
      }
    }
    return true;
  }
  
  public static void testOneExampleOfMinimization(Random rnd) {
    int nStates=100, nSymbols=2;
    int[][] transitions=new int[nStates][nSymbols];
    for(int i=0; i<nStates; i++) {
      transitions[i][0]=rnd.nextInt(nStates/2);
      transitions[i][1]=rnd.nextInt(nStates/2)+nStates/2;
    }
    Set<Integer> acceptingStates=new HashSet<>();
    for(int i=0; i<nStates/2; i++)
      acceptingStates.add(i);
    FSM f1=new FSM(transitions, acceptingStates);
    FSM f2=minimized(transitions, acceptingStates);
    if (f2.nStates()>2) {
      System.out.println("Oups... Some equivalent states remain undetected");
      System.exit(-1);
    }
    if(!seemEquivalent(f1, f2)) {
      System.out.println("Oups... Minimized FSM differs from original");
      System.exit(0);
    }

  }
  
  public static void testRndMinimization(int nStates, int nSymbols, Random rnd) {
    int nFSM=100;
    long nStatesMin=0;
    for(int i=0; i<nFSM; i++) {
      int[][] transitions=rndTransitions(nStates, nSymbols, rnd); 
      Set<Integer> acceptingStates=rndAcceptingStates(nStates, rnd);
      FSM a=new FSM(transitions, acceptingStates);
      FSM b=minimized(transitions, acceptingStates);
      nStatesMin+=b.nStates();
      if(!seemEquivalent(a,b)) {
        System.out.println("Oups... Minimized FSM differs from original");
        System.exit(0);
      }
    }
    System.out.print("avg nb of states before/after minimization: "+nStates);
    System.out.println(" / "+nStatesMin/(double)nFSM);
  }
  
  public static void main(String... args) {
    Random rnd=new Random();
    testOneExampleOfMinimization(rnd);
    testRndMinimization(3, 2, rnd);
    testRndMinimization(5, 2, rnd);
    testRndMinimization(10, 3, rnd);
    System.out.println("Test passed successfully");
  }
}
