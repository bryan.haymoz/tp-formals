package s03;

import java.util.HashSet;
import java.util.Set;
public class FSM {
  private int[][] transition;  // copy of the transition table
  // TODO - A COMPLETER
  private Set<Integer> acceptingStates;

  public FSM(int[][] transitions,        // also gives n and m
             Set<Integer> acceptingStates) {
    int nStates  = transitions.length;
    int nSymbols = transitions[0].length;
    // TODO - A COMPLETER
    this.acceptingStates = acceptingStates;
    this.transition = transitions;
  }

  public boolean accepts(int[] word) {
    // ------ Simulation pseudo-code :
    // crtState = 0;
    // for each symbol s in word
    //   apply transition with s
    // return true if crtState is accepting

    int crtState = 0;

    for (int s : word) {
      crtState = transition[crtState][s];
      //System.out.println("Symbol="+s+" State="+crtState);
    }
    return acceptingStates.contains(crtState); // check if last state is accepting
  }

  public int nStates() {
    return transition.length;
  }

  public int nSymbols() {
    return transition[0].length;
  }
  // -----------------------------------------------------------
  public static void main(String [] args) {
    if (args.length==0)
      args = new String[] {"abbab", "baab", "bbabbb"};

    /* OLD VERSION
    int [][] transitions = {
      {1, 0},       // <-- from state 0, with symbol a,b,c...
      {2, 1},       // <-- from state 1, with symbol a,b,c...
      {2, 2}
    };
    Set<Integer> acceptingStates = new HashSet<>();
    acceptingStates.add(1);
     */

    //c.f diagram on paper
    int[][] transitions = {
            {2, 1},
            {2, 1},
            {3, 1},
            {3, 3}
    };

    Set<Integer> acceptingStates = new HashSet<>();
    acceptingStates.add(0);//Add accepting state (empty state)
    acceptingStates.add(1);//Add accepting state (other state ok)
    FSM autom = new FSM(transitions, acceptingStates);

    for (int i=0; i<args.length; i++) {
      String letters = args[i];
      int [] word = new int [letters.length()];
      for (int j=0; j<letters.length(); j++)
        word[j] = letters.charAt(j) - 'a';
      boolean res = autom.accepts(word);
      System.out.println(letters +" : "+res);
    }
  }
}
